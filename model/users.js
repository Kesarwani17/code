const mongoose = require("mongoose");
const yup = require("yup");

//User Schema
const userSchema = new mongoose.Schema({
	ID: {
		type: Number,
		required: true,
	},
	name: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 1000,
	},
	age: {
		type: Number,
		required: true,
	},
});
const validateUser = (user) => {
	const schema = yup.object().shape({
		ID: yup.number().required(),
		name: yup.string().required().min(1).max(1000),
		age: yup
			.number()
			.required()
			.min(1, "Age must be greater than 1")
			.max(100, "Age must be less than 100"),
	});
	return schema
		.validate(user)
		.then((user) => user)
		.catch((err) => {
			return { message: err.message };
		});
};

const validateSearch = (user) => {
	const schema = yup.object().shape({
		name: yup.string(),
		sortOrder: yup
			.string()
			.required()
			.oneOf(
				["asc", "desc"],
				"Kindly enter correct sortOrder, asc(for ascending) and desc(for descending)"
			),
		page: yup.number().min(1, "Page must be greater than 0"),
		countPerPage: yup.number().min(1, "Page must be greater than 0"),
	});
	return schema
		.validate(user)
		.then((user) => user)
		.catch((err) => {
			return { message: err.message };
		});
};

exports.User = new mongoose.model("user", userSchema);
exports.validateUser = validateUser;
exports.validateSearch = validateSearch;
