const express = require("express");
const mongoose = require("mongoose");
const app = express();
require("dotenv").config();
const userRoute = require("./routes/user");

const port = process.env.PORT || 3000;

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//routes
app.use("/user", userRoute);

//connect to mongodb atlas
mongoose
	.connect(process.env.MONGO_URL, { useNewUrlParser: true })
	.then(() => {
		console.log("connected to db");
	})
	.catch((err) => {
		console.log("Error in Connecting DB");
	});

console.log(`serving ${port}`);

app.listen(port, () => console.log(`listening on http://localhost:${port}`));
