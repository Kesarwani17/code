const express = require("express");
const router = express.Router();
const { User, validateUser, validateSearch } = require("../model/users");

//Post: Search User
router.post("/fetch", async (req, res) => {
	let error = await validateSearch(req.body);
	let page = req.body.page || 1, //default page 1
		countPerPage = req.body.countPerPage || 5; //5 documents per page by default

	if (error && error.message) {
		res.status(400).send(error.message);
	} else {
		try {
			let totalCount, PaginatedData;

			if (req.body.name && req.body.name.length) {
				totalCount = await User.find({ name: req.body.name }).count();

				PaginatedData = await User.find(
					{ name: req.body.name },
					{ _id: 0, ID: 1, name: 1, age: 1 }
				)
					.sort({ ["age"]: req.body.sortOrder })
					.limit(countPerPage * 1)
					.skip((page - 1) * countPerPage);
			} else {
				totalCount = await User.count();

				PaginatedData = await User.find({}, { _id: 0, ID: 1, name: 1, age: 1 })
					.sort({ ["age"]: req.body.sortOrder })
					.limit(countPerPage * 1)
					.skip((page - 1) * countPerPage);
			}

			if (PaginatedData && PaginatedData.length > 0) {
				res.json({
					totalPages: Math.ceil(totalCount / countPerPage),
					CurrentPage: page,
					Data: PaginatedData,
				});
			} else {
				res.status(404).send("User Not found or invalid Pagination");
			}
		} catch (err) {
			res.status(500).send(err.message);
		}
	}
});

//Post: create users
router.post("/add", async (req, res) => {
	let error = await validateUser(req.body);

	if (error && error.message) {
		res.status(400).send(error.message);
	} else {
		let user = new User({
			ID: req.body.ID,
			name: req.body.name,
			age: req.body.age,
		});

		user
			.save()
			.then((user) => {
				res.send(user);
			})
			.catch((err) => {
				res
					.status(500)
					.send(
						`error in storing in db, id:${req.body.id}, err: ${err.message}`
					);
			});
	}
});

module.exports = router;
